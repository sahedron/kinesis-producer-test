package kprod.model;

import kprod.ProducerController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author soujisama
 */
public class NewOrderEventMessageTest {
    
    private final ProducerController instance = new ProducerController();
    
    @Before
    public void setUp() {        
    }
    
    @After
    public void tearDown() {
    }

    
    @Test
    public void generateNewOrderEventMessageTest() {
        NewOrderEventMessage eventMessage = new NewOrderEventMessage();
        String clientId = "1";
        String orderId = "2";
        eventMessage.setClientId(clientId);
        eventMessage.setOrderId(orderId);
        
        assertTrue(eventMessage.getMessageId().length() > 0);
        assertEquals(eventMessage.getOrderId(),orderId);
        assertEquals(eventMessage.getClientId(),clientId);
    }    
}
