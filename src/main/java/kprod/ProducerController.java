package kprod;

/**
 *
 * @author soujisama
 */
import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.amazonaws.services.kinesis.producer.UserRecordResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kprod.model.NewOrderEventMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducerController {

    @RequestMapping("/newOrder")
    public List<NewOrderEventMessage> generateNewOrderEventMessage(@RequestParam(value = "name", defaultValue = "Empty") String name) {

        List<NewOrderEventMessage> messages = new ArrayList<NewOrderEventMessage>();
        for (int i = 0; i < 100; ++i) {

            NewOrderEventMessage message = new NewOrderEventMessage();
            message.setClientId("1");
            message.setOrderId("01");
            
            messages.add(message);
        }

        KinesisProducer kinesis = new KinesisProducer();
        FutureCallback<UserRecordResult> myCallBack = new FutureCallback<UserRecordResult>() {

            @Override
            public void onSuccess(UserRecordResult v) {
                System.out.println(v.getSequenceNumber());
            }

            @Override
            public void onFailure(Throwable t) {
                //t.
                System.out.println(t.getMessage());
                System.out.println("Failed to add to stream");
                Logger.getLogger("Failed to add to stream");
            }

        };
        ObjectMapper mapper = new ObjectMapper();

        try {
            for (NewOrderEventMessage message : messages) {
                String messageString = mapper.writeValueAsString(message);
                System.out.println(messageString);
                ByteBuffer data = ByteBuffer.wrap(messageString.getBytes("UTF-8"));
                ListenableFuture<UserRecordResult> f = kinesis.addUserRecord("myStream", "myPartitionKey", data);
                Futures.addCallback(f, myCallBack);
            }
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProducerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            Logger.getLogger(ProducerController.class.getName()).log(Level.SEVERE, null, e);
        }

        return messages;

    }
}
