package kprod.model;

/**
 *
 * @author soujisama
 */
public class NewAccountEventMessage extends AbstractEventMessage {
    
    private String clientId;

    public NewAccountEventMessage() {
        
    }
    
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    
}
