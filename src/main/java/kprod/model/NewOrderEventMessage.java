package kprod.model;

import java.io.Serializable;

/**
 *
 * @author soujisama
 */
public class NewOrderEventMessage extends AbstractEventMessage implements Serializable {
    
    private String clientId;
    private String orderId;

    public NewOrderEventMessage() {
        super();
    }
    
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    
}
