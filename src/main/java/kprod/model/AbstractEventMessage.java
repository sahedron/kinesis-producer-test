package kprod.model;

import java.util.UUID;

/**
 *
 * @author soujisama
 */
public abstract class AbstractEventMessage {
    public String messageId;
    public String messageType;
    
    public AbstractEventMessage() {
        this.messageId = UUID.randomUUID().toString();
        this.messageType = this.getClass().getSimpleName();
    }    
    
    public String getMessageId() {
        return messageId;
    }

}
